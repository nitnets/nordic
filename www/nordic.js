var exec = require('cordova/exec');

var PLUGIN_NAME = "Nordic";

var ACCESS_FINE_LOCATION = 'android.permission.ACCESS_FINE_LOCATION';
var ACCESS_COARSE_LOCATION = 'android.permission.ACCESS_COARSE_LOCATION';

var Nordic = {
    
    update(url, onSuccess, onError) {
        exec(onSuccess, onError, PLUGIN_NAME, 'update', [url]);
    },
    requestPermission(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, PLUGIN_NAME, 'requestPermissions',
            [ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION],
        );
    },
    hasPermission(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, PLUGIN_NAME, 'hasPermission', [ACCESS_FINE_LOCATION]);
    },
}

module.exports = Nordic;
