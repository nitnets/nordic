package com.nitsnets;

interface NordicCallback {
    void showInfoMessage(String message);
    void notifyDfuStatus(String status);
    void errorMessage(String message);
    void successMessage(String message);
    void notifyProgress(int progress);
}
