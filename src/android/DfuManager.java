package com.nitsnets;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.ParcelUuid;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import no.nordicsemi.android.dfu.DfuServiceInitiator;
import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanCallback;
import no.nordicsemi.android.support.v18.scanner.ScanFilter;
import no.nordicsemi.android.support.v18.scanner.ScanResult;
import no.nordicsemi.android.support.v18.scanner.ScanSettings;

public class DfuManager {

    private static final int LOCATION_ACCESS_REQUEST_CODE = 3000;
    private static final String DFU_STATUS_MESSAGE_FORMAT = "DFU state change: %s";
    Context mContext;
    Activity mContextActivity;

    public static final int BLE_GATT_ERROR_NONE = 0;


    private final Handler dfuDiscoverHandler = new Handler();

    // This flag indicates if the DFU Service is unavailable
    private boolean dfuServiceNotFound = false;

    private boolean mDfuRetry;
    private ScanCallback bluetoothScanCallback;
    private Handler bluetoothDiscoveryTimeoutHandler;
    private BluetoothLeScannerCompat bluetoothScanner;
    private BluetoothManager bluetoothManager;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothDevice currentDevice;
    private String firmwareFilePath;
    private Uri firmawareFileUri;
    NordicCallback mNordic;

    public DfuManager(Context context, NordicCallback nordic) {
        mContext = context;
        mNordic = nordic;

        if ((context instanceof Activity)) {
            mContextActivity = (Activity) context;
        }

        // Use this check to determine whether BLE is supported on the device. Then
        // you can selectively disable BLE-related features.
        if (!mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(mContext, "This device not support BLE", Toast.LENGTH_SHORT).show();
            if (mContextActivity != null) {
                mContextActivity.finish();
            }
        }

        // Default
    }

    public boolean isLocationPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            // Related permissions
            String[] permissions = new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            };
            // Check whether permissions
            // are granted
            if (
                    ContextCompat.checkSelfPermission(mContext, permissions[0]) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(mContext, permissions[1]) != PackageManager.PERMISSION_GRANTED
            ) {
                // @see onRequestPermissionsResult below
                return false;
            }
        }
        return true;
    }


    public boolean requestPermission() {
        // Check for location permissions
        if (Build.VERSION.SDK_INT >= 23) {
            // Related permissions
            String[] permissions = new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            };
            // Check whether permissions
            // are granted
            if (
                    ContextCompat.checkSelfPermission(mContext, permissions[0]) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(mContext, permissions[1]) != PackageManager.PERMISSION_GRANTED
            ) {
                // @see onRequestPermissionsResult below
                if (mContextActivity != null) {
                    ActivityCompat.requestPermissions(mContextActivity, permissions, LOCATION_ACCESS_REQUEST_CODE);
                }

            }
        }
        return true;
    }


    @SuppressLint("StaticFieldLeak")
    public void startFirmwareUpdate(final String url) {
        mDfuRetry = false;
        currentDevice = null;
        bluetoothAdapter = bluetoothAdapter();
        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            errorMessage("The bluetooth adapter disabled");
            return;
        }
        if (!isLocationPermissionGranted()) {
            errorMessage("The location permission not granted");
            return;
        }

        try {
            //onResume should be called on Activity.onResume
            final File firmwareFile = new File(mContext.getFilesDir().getPath() + "/" + DfuService.DFU_FIRMWARE_FILENAME);

            new AsyncTask() {

                @Override
                protected Object doInBackground(Object[] objects) {
                    try {
                        return FileUtils.downloadFile(new URL(url), firmwareFile);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    return false;
                }

                @Override
                protected void onPostExecute(Object o) {
                    super.onPostExecute(o);
                    if ((Boolean) o) {
                        firmwareFilePath = firmwareFile.getAbsolutePath();
                        firmawareFileUri = Uri.fromFile(firmwareFile);
                        startBluetoothDeviceDiscovery();
                    } else {
                        errorMessage("Error downloading the file");
                    }


                }
            }.execute();


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void doFirmwareUpdate(String dfuDeviceAddress, String dfuDeviceName) {

        onResume();

        DfuServiceInitiator dfuServiceStarter = new DfuServiceInitiator(dfuDeviceAddress);


        dfuServiceStarter.setDeviceName(dfuDeviceName)
                .setKeepBond(false)
                .setForeground(false)
                .setDisableNotification(true)
                .setZip(firmawareFileUri, firmwareFilePath)
                .start(mContext, DfuService.class);
    }


    public void doFirmwareUpdate(String dfuDeviceAddress, String dfuDeviceName, String url) {

        try {
            final File firmwareFile = new File(mContext.getFilesDir().getPath() + "/" + DfuService.DFU_FIRMWARE_FILENAME);

            if (FileUtils.downloadFile(new URL(url), firmwareFile)) {
                final String firmwareFilePath = firmwareFile.getAbsolutePath();
                final Uri firmawareFileUri = Uri.fromFile(firmwareFile);

                DfuServiceInitiator dfuServiceStarter = new DfuServiceInitiator(dfuDeviceAddress);


                dfuServiceStarter.setDeviceName(dfuDeviceName)
                        .setKeepBond(false)
                        .setDisableNotification(false)
                        .setZip(firmawareFileUri, firmwareFilePath)
                        .start(mContext, DfuService.class);
            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onPause() {
        // Dfu receiver (firmware update)
        final LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(mContext);
        broadcastManager.unregisterReceiver(dfuUpdateReceiver);
    }

    public void onResume() {
        final LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(mContext);
        broadcastManager.registerReceiver(dfuUpdateReceiver, buildDfuUpdateIntentFilter());
    }

    private final BroadcastReceiver dfuUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            final String action = intent.getAction();
            // Log
            if (DfuService.BROADCAST_LOG.equals(action)) {
                // @see DfuService
                if (DfuService.SHOW_BROADCAST_LOG) {
                    // Extract log message from intent
                    String logMessage = intent.getStringExtra(DfuService.EXTRA_LOG_MESSAGE);
                    Log.d("DFU", logMessage);
                }
            }
            // Progress
            else if (DfuService.BROADCAST_PROGRESS.equals(action)) {
                int progress2 = intent.getIntExtra(DfuService.EXTRA_DATA, 0);
                Log.d("DFU TRANSFER PROGRESS", "" + progress2);
                notifyProgress(progress2);
                int progress = intent.getIntExtra(DfuService.EXTRA_DATA, 0);
                switch (progress) {
                    /**
                     * Service is connecting to the remote DFU target.
                     */
                    case DfuService.PROGRESS_CONNECTING:
                        notifyDfuStatus("Connecting");
                        break;

                    /**
                     * Service is enabling notifications and starting transmission
                     */
                    case DfuService.PROGRESS_STARTING:
                        notifyDfuStatus("Starting");
                        break;

                    /**
                     * Service has triggered a switch to bootloader mode. Now the service
                     * waits for the link loss event (this may take up to several seconds) and will connect again
                     * to the same device, now started in the bootloader mode.
                     */
                    case DfuService.PROGRESS_ENABLING_DFU_MODE:
                        notifyDfuStatus("Enabling DFU mode");
                        break;
                    /**
                     * Service is sending validation request to the remote DFU target.
                     */
                    case DfuService.PROGRESS_VALIDATING:
                        notifyDfuStatus("Validating");
                        break;

                    /**
                     * Service is disconnecting from the DFU target.
                     */
                    case DfuService.PROGRESS_DISCONNECTING:
                        notifyDfuStatus("Disconnecting");
                        break;

                    /**
                     * The connection is successful.
                     */
                    case DfuService.PROGRESS_COMPLETED:
                        notifyDfuStatus("Completed");
                        successMessage("DFU Update did finish OK");
                        onPause();
                        break;

                    /**
                     * The upload has been aborted. Previous software version will be restored on the target.
                     */
                    case DfuService.PROGRESS_ABORTED:
                        notifyDfuStatus("Aborted");
                        onPause();
                        break;
                }
            }
            // Error
            else if (DfuService.BROADCAST_ERROR.equals(action)) {
                // Avoid twice
                // Error type & code
                int errorType = intent.getIntExtra(DfuService.EXTRA_ERROR_TYPE, -1);
                int errorCode = intent.getIntExtra(DfuService.EXTRA_DATA, -1);

                // Default message
                String errorMessage = "Unexptected";

                switch (errorType) {
                    case DfuService.ERROR_TYPE_OTHER:
                        errorMessage = "Other";
                        break;

                    case DfuService.ERROR_TYPE_COMMUNICATION_STATE:
                        errorMessage = "Communication state";
                        break;

                    case DfuService.ERROR_TYPE_COMMUNICATION:
                        errorMessage = "Device disconnected unexpectly";
                        break;

                    case DfuService.ERROR_TYPE_DFU_REMOTE:
                        errorMessage = "DFU remote";
                        break;
                }
                errorMessage(String.format("Error %d. %s", errorCode, errorMessage));


                // DFU service does not exists on the device: DFU mode was activated and the name of device has been changed
                if (!dfuServiceNotFound && errorCode == DfuService.ERROR_SERVICE_NOT_FOUND) {
                    // Change button text
                    showMessage("device not found: restart");

                    // Activate DFU mode flag
                    dfuServiceNotFound = true;

                    // DFU mode changes device's name
                    dfuDiscoverHandler.post(
                            new Runnable() {
                                @Override
                                public void run() {
                                    startBluetoothDeviceDiscovery();
                                }
                            }
                    );
                }
                // Show error message
                else {
                    showMessage("DFU connection error");
                    errorMessage("DFU connection error");

                    // Deactivate DFU mode flag
                    dfuServiceNotFound = false;

                    mDfuRetry = true;
                    retryDiscover();
                }

                onPause();

            }
        }
    };

    private void retryDiscover() {
//        if (!bluetoothAdapter().isDiscovering()) {
//            currentDevice = null;
//
//            startBluetoothDeviceDiscovery();
//        }
    }


    private void successMessage(final String message) {
        mContextActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mNordic.successMessage(message);
            }
        });

    }

    private void notifyProgress(final int progress2) {
        mContextActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mNordic.notifyProgress(progress2);
            }
        });

    }


    private void notifyDfuStatus(final String status) {
        mContextActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mNordic.notifyDfuStatus(String.format(DFU_STATUS_MESSAGE_FORMAT, status));
            }
        });

    }

    private void errorMessage(final String message) {
        mContextActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mNordic.errorMessage(message);
            }
        });

    }


    private void startBluetoothDeviceDiscovery() {


        // Names to discover
        final String[] matchDeviceNames = new String[]{
                "SCU_DFU_1"
        };

        // Start BT device discovery
        startBluetoothDeviceDiscovery(
                matchDeviceNames,
                new BluetoothDiscoveryCallback() {
                    @Override
                    public void onDiscovered(BluetoothDevice device) {
                        doFirmwareUpdate(device.getAddress(), device.getName());
                    }

                    @Override
                    public void onStart() {
//                        showMessage("scanning");
                        // Utils.playSound(context(), R.raw.your_turn);
                    }

                    @Override
                    public void onStop() {
                        errorMessage("DFU device not found");
                        if (mDfuRetry) {
                            retryDiscover();
                        }
                    }

                    @Override
                    public void onError(String errorMessage) {

                        //new AlertDialog.Builder(WelcomeActivity.this).setMessage(errorMessage).show();
                        Log.d("ERROR", errorMessage);
                        // Show the error message
                        showMessage("error discovering");

                    }
                },
                10000   // 10 secs
        );
    }


    public void startBluetoothDeviceDiscovery(
            final String[] matchDeviceNames,
            final BluetoothDiscoveryCallback callback,
            long delay
    ) {
        // Avoid call more than once
        // Adapter
        BluetoothAdapter bluetoothAdapter = bluetoothAdapter();
        if (bluetoothAdapter == null) {
            callback.onError("Unable to start Bluetooth adapter.");
        } else {
            mContextActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    bluetoothDiscoveryTimeoutHandler = new Handler();
                }
            });
            bluetoothScanner = BluetoothLeScannerCompat.getScanner();
            // New API?
            bluetoothScanCallback = new ScanCallback() {
                @Override
                public void onScanResult(int callbackType, ScanResult scanResult) {
                    super.onScanResult(callbackType, scanResult);
                    Log.d("DFU SERVICE", scanResult.getScanRecord().toString());
                    if (currentDevice == null) {
                        mDfuRetry = false;
                        // Get current device
                        BluetoothDevice device = scanResult.getDevice();
                        stopBluetoothDeviceDiscovery();
                        currentDevice = device;

                        // Notify discovered device
                        callback.onDiscovered(device);
                    }
                }

                @Override
                public void onBatchScanResults(List<ScanResult> scanResultsList) {
                    super.onBatchScanResults(scanResultsList);
                    if (scanResultsList != null && scanResultsList.size() > 0) {
                        // Get first device
                        BluetoothDevice device = scanResultsList.get(0).getDevice();
                        stopBluetoothDeviceDiscovery();

                        // Notify first discovered device
                        callback.onDiscovered(device);
                    }
                }

                @Override
                public void onScanFailed(int errorCode) {
                    super.onScanFailed(errorCode);
                    // Notify error
                    callback.onError("SCAN_FAILED_" + errorCode);
                }
            };

            List<ScanFilter> scanFilterList = Arrays.asList(
                    new ScanFilter[]{
                            new ScanFilter.Builder()
                                    .setServiceUuid(ParcelUuid.fromString("0000fe59-0000-1000-8000-00805f9b34fb"))
                                    .build(),
                            new ScanFilter.Builder()
                                    .setServiceUuid(ParcelUuid.fromString("0000180A-0000-1000-8000-00805f9b34fb"))
                                    .build()
                            ,
                            new ScanFilter.Builder()
                                    .setServiceUuid(ParcelUuid.fromString("00001530-1212-EFDE-1523-785FEABCD123".toLowerCase()))
                                    .build()

                    });

            // Add all devices
//                for (String deviceName : matchDeviceNames) {
//                    ScanFilter scanFilter = new ScanFilter.Builder().setDeviceName(deviceName).build();
//                    scanFilterList.add(scanFilter);
//                }

            ScanSettings scanSettings = new ScanSettings.Builder().build();

            bluetoothScanner.startScan(scanFilterList, scanSettings, bluetoothScanCallback);

            callback.onStart();

            // Scan timeout handler and its runnable
            bluetoothDiscoveryTimeoutHandler.postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {
                            stopBluetoothDeviceDiscovery();
                            callback.onStop();
                        }
                    },
                    delay
            );
        }
    }

    public synchronized void stopBluetoothDeviceDiscovery() {
        // Remove callbacks
        mContextActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (bluetoothDiscoveryTimeoutHandler != null) {
                    bluetoothDiscoveryTimeoutHandler.removeCallbacksAndMessages(null);
                }
            }
        });

        if (bluetoothScanner != null && bluetoothScanCallback != null) {
            bluetoothScanner.stopScan(bluetoothScanCallback);
        }

        // Marked for GC
        bluetoothScanCallback = null;
        bluetoothScanner = null;
        bluetoothDiscoveryTimeoutHandler = null;
    }


    // Timeout handler

    public BluetoothAdapter bluetoothAdapter() {
        if (bluetoothAdapter == null) {
            bluetoothAdapter = bluetoothManager().getAdapter();
        }
        return bluetoothAdapter;
    }

    public BluetoothManager bluetoothManager() {
        if (bluetoothManager == null) {
            bluetoothManager = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        }
        return bluetoothManager;
    }


    /**
     * Check whether the discovered device matchs with the expected
     */
    private boolean foundBluetoothDevice(String lastBluetoothDeviceName, BluetoothDevice device, String[] matchDeviceNames) {
        final String currentDeviceName = (device == null) ? null : device.getName();
        if (currentDeviceName != null && !currentDeviceName.equals(lastBluetoothDeviceName)) {
            for (String deviceName : matchDeviceNames) {
                if (currentDeviceName.equals(deviceName)) {
                    return true;
                }
            }
        }
        return false;

    }


    private void showMessage(final String message) {
        mContextActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mNordic.showInfoMessage(message);
            }
        });

    }

    private static IntentFilter buildDfuUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction(DfuService.BROADCAST_PROGRESS);
        intentFilter.addAction(DfuService.BROADCAST_ERROR);
        intentFilter.addAction(DfuService.BROADCAST_LOG);

        return intentFilter;
    }
}
