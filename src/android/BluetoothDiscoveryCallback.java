package com.nitsnets;

import android.bluetooth.BluetoothDevice;


public interface BluetoothDiscoveryCallback {
    public void onDiscovered(BluetoothDevice device);
    public void onStart();
    public void onStop();
    public void onError(String errorMessage);
}
