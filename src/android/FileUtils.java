package com.nitsnets;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;


public class FileUtils {

    public static boolean downloadFile(URL url, File destFile) {
        try {
            ReadableByteChannel readableByteChannel = Channels.newChannel(url.openStream());
            FileOutputStream fileOutputStream = new FileOutputStream(destFile);
            fileOutputStream.getChannel()
                    .transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
    /**
     * Copy data from a source stream to destFile.
     * Return true if succeed, return false if failed.
     */
    public static boolean copyFile(InputStream inputStream, File destFile) {
        try {
            if (destFile.exists()) {
                destFile.delete();
            }

            OutputStream outputStream = new FileOutputStream(destFile);
            byte[] buffer;

            try {
                buffer = new byte[4096];

                int bytesRead;
                while ((bytesRead = inputStream.read(buffer)) >= 0) {
                    outputStream.write(buffer, 0, bytesRead);
                }
            }
            finally {
                if (outputStream != null) {
                    outputStream.close();
                }
                // Marked for GC
                outputStream = null;
                buffer = null;
            }

            return true;
        }
        catch (IOException e) {
            return false;
        }
    }
}
