package com.nitsnets;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Vibrator;

/**
 * Utilities.
 *
 * @author Antonio de Sousa Barroso
 */
public class Utils {
    public static ConnectivityManager connectivityManager = null;

    public static String getAppVersionName(final Context context) {
        PackageInfo packageInfo = null;

        try {
            packageInfo = context.getPackageManager().getPackageInfo(
                    context.getPackageName(),
                    PackageManager.GET_META_DATA
            );
            return packageInfo.versionName;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            // Marked for GC
            packageInfo = null;
        }

        return "";
    }

    public static boolean isEmptyOrNull(String s) {
        return (s == null) || (s.trim().length() == 0);
    }

    public static boolean isEmpty(String s) {
        return (s == null) || (s.trim().length() == 0);
    }

    public static boolean isNotEmptyOrNull(String s) {
        return (s != null) && (s.trim().length() > 0);
    }

    public static boolean isNotEmpty(String s) {
        return (s != null) && (s.trim().length() > 0);
    }

    // Connectivity

    public static boolean isActiveNetworkConnection(final Context context) {
        if (connectivityManager == null) {
            connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        }

        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null) {
            return false;
        } else {
            return networkInfo.isConnectedOrConnecting();
        }
    }

    public static boolean isGPSEnabled(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    // Vibrator

    private static Vibrator vibrator;
    public static final Vibrator getVibrator(Context context) {
        if (vibrator == null) {
            vibrator = ((Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE));
        }
        return vibrator;
    }

    public static void vibrate(final Context context, long time) {
        Vibrator vibrator = Utils.getVibrator(context);
        if (vibrator != null) {
            vibrator.vibrate(time);
        }
    }

    public static String avoidNull(final String s) {
        return avoidNull(s, "");
    }

    public static String avoidNull(final String s, final String defaultValue) {
        if (s == null || "null".equals(s)) {
            return defaultValue;
        }
        return s;
    }

    public static String avoidEmptyOrNull(final String s, final String defaultValue) {
        if (Utils.isEmptyOrNull(s) || "null".equals(s)) {
            return defaultValue;
        }
        return s;
    }


    /**
     * Play Sound
     */
    public static void playSound(final Context context, int soundResourceID) {
        try {
            MediaPlayer player = new MediaPlayer();
            // Max volume
            player.setVolume(1.0f, 1.0f);
            player.setDataSource(context, Uri.parse("android.resource://" + context.getPackageName() + "/" + soundResourceID));

            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    mediaPlayer.release();
                }
            });

            player.prepare();
            player.start();
        }
        catch (Exception e) {
            // Ignored
        }
    }
}