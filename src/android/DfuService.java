package com.nitsnets;
import android.app.Activity;

import no.nordicsemi.android.dfu.DfuBaseService;

/**
 * DFU Base Service
 */
public class DfuService extends DfuBaseService {
    // Enable/Disable show a log message during DFU process
    public static final boolean SHOW_BROADCAST_LOG = true;

    // Firmware file
    public static final String DFU_FIRMWARE_FILENAME = "dfu_.zip";
    public static final String DFU_FIRMWARE_FILE_PATH = "firmware/" + DFU_FIRMWARE_FILENAME;

    @Override
    protected Class<? extends Activity> getNotificationTarget() {
        return Activity.class;
    }
}
