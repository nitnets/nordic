//
//  DFUCompleteManager.swift
//  iOSDFULibrary_Example
//
//  Created by Jose Miguel Alcaraz on 30/04/2019.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import Foundation
import CoreBluetooth
import iOSDFULibrary



public protocol DFUCompleteManagerDelegate {
    func dfuUpdatedDidFinishOK()
    func dfuUpdatedDidFinishWithError(error: NSString)
    func dfuUpdateProgress(progress: Float)
    func dfuState (state: DFUState)
}



public class DFUCompleteManager: NSObject, CBCentralManagerDelegate, DFUServiceDelegate, DFUProgressDelegate, LoggerDelegate {
    
    static var legacyDfuServiceUUID  = CBUUID(string: "00001530-1212-EFDE-1523-785FEABCD123")
    static var secureDfuServiceUUID  = CBUUID(string: "FE59")
    static var deviceInfoServiceUUID = CBUUID(string: "180A")
    static var TIME_OUT_DISCOVERY = 10.0
    
    //MARK: - Class Properties
    private var dfuPeripheral    : CBPeripheral!
    private var peripheralName   : String?
    private var dfuController    : DFUServiceController!
    private var centralManager   : CBCentralManager!
    private var currentDestinationZipURL    : URL?
    private var originZipURL     : String?
    
    var scanningStarted             : Bool = false
    
    var timer: Timer?
    
    public var delegate: DFUCompleteManagerDelegate?
    
    
    public init(peripheralID: String, zipURL: String, delegate: DFUCompleteManagerDelegate)
    {
        super.init()
        originZipURL = zipURL
        self.delegate = delegate
        self.centralManager = CBCentralManager(delegate: self, queue: nil)
    }
    
    //MARK: - DFU Methods
    
    func startDFUProcess() {
        guard let dfuPeripheral = dfuPeripheral else {
            //            print("No DFU peripheral was set")
            self.delegate?.dfuUpdatedDidFinishWithError(error: "DFU device not found")
            return
        }
        
        // Create DFU initiator with some default configuration
        let dfuInitiator = DFUServiceInitiator(queue: DispatchQueue(label: "Other"))
        dfuInitiator.delegate = self
        dfuInitiator.progressDelegate = self
        dfuInitiator.logger = self
        
        if #available(iOS 11.0, macOS 10.13, *) {
            dfuInitiator.packetReceiptNotificationParameter = 0
        }
        
        let firmware = DFUFirmware(urlToZipFile: currentDestinationZipURL!, type: .softdeviceBootloaderApplication)
        
        dfuController = dfuInitiator.with(firmware: firmware!).start(target: dfuPeripheral)
    }
    
    
    func startDiscovery()
    {
        if !scanningStarted
        {
            scanningStarted = true
            //            print("Start discovery")
            // By default the scanner shows only devices advertising with one of those Service UUIDs:
            centralManager!.delegate = self
            centralManager!.scanForPeripherals(
                withServices: [
                    DFUCompleteManager.legacyDfuServiceUUID,
                    DFUCompleteManager.secureDfuServiceUUID,
                    DFUCompleteManager.deviceInfoServiceUUID],
                options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
            
            if timer == nil
            {
                timer = Timer.scheduledTimer(timeInterval: DFUCompleteManager.TIME_OUT_DISCOVERY, target: self, selector: #selector(DFUCompleteManager.discoveryTimeout), userInfo: nil, repeats: false)
            }
        }
    }
    
    
    
    @objc private func discoveryTimeout()
    {
        scanningStarted = false
        centralManager.stopScan()
        self.delegate?.dfuUpdatedDidFinishWithError(error: "DFU device not found")
        //        print("salt el time out")
    }
    
    func stopDiscoveryTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    
    
    //MARK: - CBCentralManagerDelegate
    
    public func centralManagerDidUpdateState(_ central: CBCentralManager)
    {
        if central.state == .poweredOn
        {
            //            print("Central Manager is now powered on")
            startDiscovery()
        }
    }
    
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber)
    {
        scanningStarted = false
        centralManager.stopScan()
        dfuPeripheral = peripheral
        
        stopDiscoveryTimer()
        
        downloadFromUrl(url: originZipURL!)
    }
    
    //MARK: - DFUServiceDelegate
    
    public func dfuStateDidChange(to state: DFUState) {
        
        //        print("State changed to: \(state.description())")
        self.delegate?.dfuState(state: state)
        // Forget the controller when DFU is done
        if state == .completed {
            dfuController = nil
            self.delegate?.dfuUpdatedDidFinishOK()
        }
    }
    
    public func dfuError(_ error: DFUError, didOccurWithMessage message: String) {
        
        //        print("Error \(error.rawValue): \(message)")
        self.delegate?.dfuUpdatedDidFinishWithError(error: "Error \(error.rawValue). \(message)" as NSString)
        
        // Forget the controller when DFU finished with an error
        dfuController = nil
        
    }
    
    //MARK: - DFUProgressDelegate
    
    public func dfuProgressDidChange(for part: Int, outOf totalParts: Int, to progress: Int, currentSpeedBytesPerSecond: Double, avgSpeedBytesPerSecond: Double) {
        // Update the total progress view
        
        //        print("Progress: \(Float(progress))")
        self.delegate?.dfuUpdateProgress(progress: Float(progress))
        
    }
    
    //MARK: - LoggerDelegate
    
    public func logWith(_ level: LogLevel, message: String) {
        //if level.rawValue >= LogLevel.application.rawValue {
        //        print("\(level.name()): \(message)")
        //}
    }
    
    //MARK: - Download Methods
    //    "https://sample-videos.com/zip/10mb.zip"
    public func downloadFromUrl(url: String){
        // Create destination URL
        
        
        let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let time = NSDate().timeIntervalSince1970
        let zipName = String(format: "dfuFile_%0.0f.zip", time)
        
        
        let destinationFileUrl = documentsUrl.appendingPathComponent(zipName)
        currentDestinationZipURL = destinationFileUrl
        
        //Create URL to the source file you want to download
        let fileURL = URL(string: url)
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        
        let request = URLRequest(url:fileURL!)
        
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success                
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    self.startDFUProcess()
                } catch (let writeError) {
                    // print("Error creating a file \(destinationFileUrl) : \(writeError)")
                    self.delegate?.dfuUpdatedDidFinishWithError(error: "Error creating DFU file in local: \(writeError)" as NSString)
                }
                
            } else {
                // print("Error took place while downloading a file. Error description: %@", error?.localizedDescription ?? "Unkown error");
                self.delegate?.dfuUpdatedDidFinishWithError(error: "Error took place while downloading DFU file")
            }
        }
        task.resume()
    }
}
