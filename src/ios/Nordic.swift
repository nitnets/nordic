/*
 * Notes: The @objc shows that this class & function should be exposed to Cordova.
 */
import iOSDFULibrary

@objc(Nordic) class Nordic : CDVPlugin, DFUCompleteManagerDelegate {
    
    var dfucm :DFUCompleteManager!
    var command :CDVInvokedUrlCommand!
    @objc(update:)
    func update(_ command: CDVInvokedUrlCommand) {
        self.command = command
        let zipURL = command.arguments[0] as? String ?? ""
        dfucm = DFUCompleteManager(peripheralID: "", zipURL: zipURL, delegate: self)
    }
    
    
    
    func dfuUpdatedDidFinishOK()
    {
        print("[FINISH OK]")
        
        let pluginResult: CDVPluginResult
        pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "DFU Update did finish OK")
        pluginResult.setKeepCallbackAs(false)
        commandDelegate!.send(pluginResult, callbackId: self.command.callbackId)
    }
    
    func dfuUpdatedDidFinishWithError(error: NSString)
    {
        print("[ERROR: \(error)]")
        
        let pluginResult: CDVPluginResult
        pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "DFU did finish by: \(error)")
        pluginResult.setKeepCallbackAs(false)
        commandDelegate!.send(pluginResult, callbackId: self.command.callbackId)
    }
    
    func dfuUpdateProgress(progress: Float)
    {
        print("[PROGRESS: \(progress)]")

        let pluginResult: CDVPluginResult
        pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "\(Int(progress))")
        pluginResult.setKeepCallbackAs(true)
        commandDelegate!.send(pluginResult, callbackId: self.command.callbackId)
    }
    
    func dfuState(state: DFUState)
    {
        print("[STATE CHANGE: \(state.description())]")
        
        let pluginResult: CDVPluginResult
        pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "DFU state change: \(state.description())")
        pluginResult.setKeepCallbackAs(true)
        commandDelegate!.send(pluginResult, callbackId: self.command.callbackId)
    }
    
}
